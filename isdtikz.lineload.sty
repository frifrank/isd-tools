% ---------------------------------------------------------------------------- %
%                                Shear line load                               %
% ---------------------------------------------------------------------------- %

\newif\ifliloswap
\pgfqkeys{/tikz/lineload}{
    from/.store in=\lilo@from,
    to/.store in=\lilo@to,
    sep/.store in=\lilo@sep,
    step/.store in=\lilo@step,
    lin start/.store in = \lilo@lin@start,
    lin end/.store in = \lilo@lin@end,
    linear/.style 2 args={lin start=#1, lin end=#2},
    const/.style={linear={#1}{#1}},
    trim start/.store in=\lilo@trim@s,
    trim end/.store in=\lilo@trim@e,
    trim/.style={trim start={#1}, trim end={#1}},
    no trim/.style={trim=0cm},
    no start/.style={start@line/.style={draw=none}, trim start=0cm},
    no end/.style={end@line/.style={draw=none}, trim end=0cm},
    shift start/.store in=\lilo@shift@s,
    shift end/.store in=\lilo@shift@e,
    shift/.style 2 args={shift start={#1}, shift end={#2}},
    tilt/.store in=\lilo@tilt,
    swap/.is if=liloswap,
    in/.store in=\lilo@lin@in,
    out/.store in=\lilo@lin@out,
    bend/.style 2 args={out=#1, in=#2},
    looseness/.store in=\lilo@looseness,
    function/.style={/tikz/declare function={templilofunc(\x) = {#1};}},
    function/.default={\x},
    start label/.style={/tikz/lineload/start label settings/.cd, #1, /tikz/lineload/.cd,},
    slabel/.style 2 args={start label={text={#1}, pos={#2}}},
    svalue/.style 2 args={start label={text={\num{#1}}, pos={#2}}},
    end label/.style={/tikz/lineload/end label settings/.cd, #1, /tikz/lineload/.cd,},
    elabel/.style 2 args={end label={text={#1}, pos={#2}}},
    evalue/.style 2 args={end label={text={\num{#1}}, pos={#2}}},
    default/.style={from={(0,0)}, to={(1,0)}, sep=5pt, step=0.4cm, normal=false, normal sep=5pt, 
        linear={}{}, in={}, out={}, looseness=0.75, tilt=0, shift={0cm}{0cm}, swap=false, trim=5pt,
        start@line/.style={draw}, end@line/.style={draw},
        start label={text=, pin pos=0.5, pin options=, pos=above left, distance=.5cm},
        end label={text=, pin pos=0.5, pin options=, pos=above right, distance=.5cm},
    },
}

\tikzset{
    every lineload/.style={isdred, semithick},
    every lineload pin/.style={isdred},
    every lineload pin edge/.style={isdred},
}

\pgfqkeys{/tikz/lineload/start label settings}{
    text/.store in=\lilo@sl@text,
    pos/.store in=\lilo@sl@pos,
    pin pos/.store in=\lilo@sl@pinpos,
    pin options/.store in=\lilo@sl@pinoptions,
    distance/.store in=\lilo@sl@distance,
}

\pgfqkeys{/tikz/lineload/end label settings}{
    text/.store in=\lilo@el@text,
    pos/.store in=\lilo@el@pos,
    pin pos/.store in=\lilo@el@pinpos,
    pin options/.store in=\lilo@el@pinoptions,
    distance/.store in=\lilo@el@distance,
}

\newif\ifnormallineload
\pgfqkeys{/tikz/lineload}{
    normal/.is if=normallineload, 
    normal=false,
    normal sep/.store in=\lilo@normal@sep,
}

\newcommand{\lineload@calc@common}{
    % save length and angle of line in macro
    \begin{pgfinterruptboundingbox}
        \path ($(\lilo@to)-(\lilo@from)$); 
    \end{pgfinterruptboundingbox}
    \pgfgetlastxy{\lilo@xdiff}{\lilo@ydiff};

    % get global scale 
    \pgfgettransformentries{\tmpa}{\tmpb}{\tmpc}{\tmpd}{\tmp}{\tmp}%
    \pgfmathsetmacro\global@scale{sqrt(\tmpa*\tmpd-\tmpb*\tmpc)}

    % scale coordinates to match the "real" distance
    \pgfmathsetmacro{\lilo@xdiff}{\lilo@xdiff/\global@scale}
    \pgfmathsetmacro{\lilo@ydiff}{\lilo@ydiff/\global@scale}

    \pgfmathsetmacro\lilo@len{veclen(\lilo@xdiff, \lilo@ydiff)/1cm-\lilo@trim@s/1cm-\lilo@trim@e/1cm}
    \pgfmathsetmacro\lilo@ang{atan2(\lilo@ydiff/1cm, \lilo@xdiff/1cm)}
}

\newcommand{\lineload@calc@shearsteps}{
    % steps for shear load
    \pgfmathsetmacro\lilo@shiftedlen{\lilo@len-\lilo@shift@s/1cm-\lilo@shift@e/1cm}
    \pgfmathsetmacro\lilo@realstep{\lilo@shiftedlen/round(\lilo@shiftedlen/(\lilo@step/1cm))}
    \pgfmathsetmacro\lilo@firststep{0.5*\lilo@realstep+\lilo@shift@s/1cm}
    \pgfmathsetmacro\lilo@secondstep{1.5*\lilo@realstep+\lilo@shift@s/1cm}
    \pgfmathsetmacro\lilo@laststep{\lilo@len-0.5*\lilo@realstep-\lilo@shift@e/1cm}
    }
    
\newcommand{\lineload@calc@normalsteps}{
    % steps for normal load
    \pgfmathsetmacro\lilo@shiftedlen{\lilo@len-\lilo@shift@s/1cm-\lilo@shift@e/1cm+\lilo@normal@sep/1cm}
    \pgfmathsetmacro\lilo@realstep{\lilo@shiftedlen/round(\lilo@shiftedlen/(\lilo@step/1cm+\lilo@normal@sep/1cm))}
    \pgfmathsetmacro\lilo@arrowlength{\lilo@realstep-\lilo@normal@sep/1cm}
    \pgfmathsetmacro\lilo@firststep{\lilo@shift@s/1cm}
    \pgfmathsetmacro\lilo@secondstep{\lilo@realstep+\lilo@shift@s/1cm}
    \pgfmathsetmacro\lilo@laststep{\lilo@len+\lilo@normal@sep/1cm-\lilo@realstep-\lilo@shift@e/1cm}
}

\newcommand{\lineload@coordinates}{
    \coordinate (lilo@aux) at ($(\lilo@from)!\lilo@trim@s!(\lilo@to)$);
    \coordinate (lilo@sep@from) at ($(lilo@aux)!{\lilo@sep}!{\lilo@swap@factor*(90+\lilo@tilt)}:(\lilo@to)$);
    \coordinate (lilo@aux) at ($(\lilo@to)!\lilo@trim@e!(\lilo@from)$);
    \coordinate (lilo@sep@to)   at ($(lilo@aux)!{\lilo@sep}!{\lilo@swap@factor*(-90+\lilo@tilt)}:(\lilo@from)$);
}

\newcommand{\lineload@normalarrows}{
    \foreach \@step in {\lilo@firststep,\lilo@secondstep,...,\lilo@laststep}{
        \draw[every lineload, ->] ($(lilo@sep@from)!\@step cm!(lilo@sep@to)$) coordinate (tmp)
            -- +(\lilo@ang:\lilo@arrowlength cm);
    }
}

\newcommand{\lineload@sheararrows}{
    \foreach \@step in {\lilo@firststep,\lilo@secondstep,...,\lilo@laststep}{
        \draw[every lineload, <-] ($(lilo@sep@from)!\@step cm!(lilo@sep@to)$) -- +({\lilo@swap@factor*(90+\lilo@tilt+\lilo@ang)}:5cm);
    }
}

\newcommand{\lineload@lin@plot}{
    \coordinate (lilo@lin@start) at ($(lilo@sep@from)!\lilo@lin@start cm!{\lilo@swap@factor*(90+\lilo@tilt)}:(lilo@sep@to)$);
    \coordinate (lilo@slabel) at ($(lilo@sep@from)!\lilo@sl@pinpos!(lilo@lin@start)$);

    \coordinate (lilo@lin@end) at ($(lilo@sep@to)!\lilo@lin@end cm!{\lilo@swap@factor*(-90+\lilo@tilt)}:(lilo@sep@from)$);
    \coordinate (lilo@elabel) at ($(lilo@sep@to)!\lilo@el@pinpos!(lilo@lin@end)$);

    \ifboolexpe{%
        test {\ifdefempty{\lilo@lin@in}} and 
        test {\ifdefempty{\lilo@lin@out}}%
    }{
        % no bending
        \draw[every lineload] (lilo@sep@from)  edge[lineload/start@line] (lilo@lin@start)
                                (lilo@lin@start) -- (lilo@lin@end) 
                                (lilo@lin@end) edge[lineload/end@line] (lilo@sep@to) 
                                (lilo@sep@to) --  (lilo@sep@from);
        \clip (lilo@sep@from) -- (lilo@lin@start) -- (lilo@lin@end) -- (lilo@sep@to) ;
    }{
        % with bending
        \draw[every lineload] (lilo@sep@from)  edge[lineload/start@line] (lilo@lin@start)
            (lilo@lin@start) to[out={\lilo@lin@out + \lilo@ang}, in={\lilo@lin@in+\lilo@ang+180}, looseness=\lilo@looseness] 
            (lilo@lin@end) (lilo@lin@end) edge[lineload/end@line] (lilo@sep@to) (lilo@sep@to) --  (lilo@sep@from);
        \clip (lilo@sep@from) -- (lilo@lin@start) 
            to[out={\lilo@lin@out + \lilo@ang}, in={\lilo@lin@in+\lilo@ang+180}, looseness=\lilo@looseness] 
            (lilo@lin@end) -- (lilo@sep@to) ;
    }
}

\newcommand{\lineload@func@plot}{
    \draw[every lineload] (lilo@sep@from) --  
        plot[shift={(lilo@sep@from)},rotate=\lilo@ang, domain=0:\lilo@len,samples=100] (\x,{\lilo@swap@factor*templilofunc(\x)})  
        -- (lilo@sep@to) -- cycle;
    \clip (lilo@sep@from) -- 
        plot[shift={(lilo@sep@from)},rotate=\lilo@ang, domain=0:\lilo@len,samples=100] (\x,{\lilo@swap@factor*templilofunc(\x)}) 
        -- (lilo@sep@to) -- cycle;
}

\newcommand{\lineload@labels}{
    % can be improved!
    \ifdefempty{\lilo@sl@text}{}
    {
        \node[pin={[
            every lineload pin, 
            pin distance=\lilo@sl@distance, 
            pin edge={every lineload pin edge},
            \lilo@sl@pinoptions, 
            pin position=\lilo@sl@pos]\lilo@sl@text}] at (lilo@slabel) {};
    }

    \ifdefempty{\lilo@el@text}{}
    {
        \node[pin={[every lineload pin, 
            pin distance=\lilo@el@distance, 
            pin edge={every lineload pin edge},
            \lilo@el@pinoptions, 
            pin position=\lilo@el@pos]\lilo@el@text}] at (lilo@elabel) {};
    }
}

\newcommand{\@lineload}[1]{
    \tikzset{lineload/.cd, default, #1}

    \ifliloswap
    \pgfmathsetmacro{\lilo@swap@factor}{-1}
    \else
    \pgfmathsetmacro{\lilo@swap@factor}{1}
    \fi 
    
    \lineload@coordinates
    \lineload@calc@common

    
    \ifnormallineload
        \lineload@calc@normalsteps
        \lineload@normalarrows
    \else
        \lineload@calc@shearsteps
        \ifboolexpe{%
            test {\ifdefempty{\lilo@lin@start}} and 
            test {\ifdefempty{\lilo@lin@end}}%
        }{
            % No linear or constant plot
            \begin{scope}
                \lineload@func@plot
                \lineload@sheararrows
            \end{scope}
        }{
            % Linear or constant plot
            \begin{scope}
                \lineload@lin@plot
                \lineload@sheararrows
            \end{scope}
            \lineload@labels
        }
    \fi
}