# ISD LaTeX Pakete

## Installation für globalen Zugriff

[Anleitung auf StackExchange wie man das richtige texmf Verzeichnis findet.](https://tex.stackexchange.com/questions/1137/where-do-i-place-my-own-sty-or-cls-files-to-make-them-available-to-all-my-te)

Die Dateien `isdbasics.sty`, `isdlayout.sty`, `isdtikz.sty`, `LogoISD.pdf` und `LogoUni.pdf` in einen Ordner (z.B. `isdpackages`) 
in das Verzeichnis `C:/Users/name/texmf/tex/latex/` (wenn nach oben stehender Anleitung vorgegangen wird) kopieren (oder das Verzeichnis dort hinein klonen).

## Nutzung
In einem LaTeX Dokument folgende Zeilen in die Präambel schreiben:
```latex
\usepackage{isdbasics}
\usepackage{isdlayout} % oder \usepackage[fancy]{isdlayout}
\usepackage{isdtikz}
```
... Fertig